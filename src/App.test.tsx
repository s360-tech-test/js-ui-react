import React from 'react';
import { render, screen } from '@testing-library/react';
import Orders from './Orders';
import Deposits from './Deposits';

test('renders orders', () => {
  render(<Orders />);
  const title = screen.getByText(/recent money/i);
  expect(title).toBeInTheDocument();
});

test('renders deposits', () => {
  render(<Deposits />);
  
  expect('').toBeInTheDocument();
});
